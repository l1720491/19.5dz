#include <iostream>

class Animal
{
public:
    void DogVoice()
    {
        std::cout << "WOOF!" << "\n";
    }
    void CatVoice()
    {
        std::cout << "MRRR MRRR MRRR!" << "\n";
    }
    void CowVoice()
    {
        std::cout << "MUUUUUU!" << "\n";
    }
};
class Dog : Animal
{

};
class Cat : Animal
{

};
class Cow : Animal
{

};

int main()
{
    Animal VoiceAnimal;
    VoiceAnimal.DogVoice();
    VoiceAnimal.CatVoice();
    VoiceAnimal.CowVoice();
}